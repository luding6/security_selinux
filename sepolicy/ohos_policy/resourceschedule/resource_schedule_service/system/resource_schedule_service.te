# Copyright (c) 2022-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

type resource_schedule_service, sadomain, domain;

#service_binder
allow resource_schedule_service accesstoken_service:binder { call };
allow resource_schedule_service foundation:binder { call transfer };
allow resource_schedule_service hiview:binder { call transfer };
allow resource_schedule_service param_watcher:binder { call transfer };
allow resource_schedule_service bgtaskmgr_service:binder { call transfer };
allow resource_schedule_service audio_policy:binder { call transfer };
allow resource_schedule_service pulseaudio:binder { call transfer };
allow resource_schedule_service msdp_sa:binder { call transfer };
allow resource_schedule_service bluetooth_service:binder { call };
allow resource_schedule_service locationhub:binder { call };
allow resource_schedule_service time_service:binder { call };
allow resource_schedule_service av_session:binder { call transfer };
allow av_session resource_schedule_service:binder { call };
allow bluetooth_service resource_schedule_service:binder { call };
allow pulseaudio resource_schedule_service:binder { call transfer };
allow audio_policy resource_schedule_service:binder { call transfer };
allow msdp_sa resource_schedule_service:binder { call transfer };
allow render_service resource_schedule_service:binder { call };
allow telephony_sa resource_schedule_service:binder { call };

#appspawn
allow resource_schedule_service appspawn:dir { search };
allow resource_schedule_service appspawn:file { getattr open read };
#appspawn_exec
allow resource_schedule_service appspawn_exec:file { open read };
#cgroup
allow resource_schedule_service cgroup:dir { add_name write search };
allow resource_schedule_service cgroup:file { append getattr ioctl open read write };
#chip_prod_file
allow resource_schedule_service chip_prod_file:dir { search };
#data_service_el1_file
allow resource_schedule_service data_service_el1_file:dir { add_name create getattr open read remove_name rmdir search write };
allow resource_schedule_service data_service_el1_file:file { create getattr ioctl lock open read unlink write };
#data_service_el2
allow resource_schedule_service data_service_el2_file:dir { search };
allow resource_schedule_service data_service_el2_hmdfs:dir { search };
#data_service_file
allow resource_schedule_service data_service_file:dir { search };
#device_manager
allow resource_schedule_service device_manager:file { read };
#hdf_devmgr
allow resource_schedule_service hdf_devmgr:file { read };
#hiview
allow hiview resource_schedule_service:dir { getattr };
allow hiview resource_schedule_service:file { getattr read };
#hilogd
allow resource_schedule_service hilogd:file { read };
#vendor
allow resource_schedule_service vendor_bin_file:dir { search };
allow resource_schedule_service vendor_lib_file:dir { search };
allow resource_schedule_service vendor_lib_file:file { execute getattr map open read };
allow resource_schedule_service vendor_file:dir { search };
allow resource_schedule_service vendor_file:file { execute getattr map open read };
allow resource_schedule_service vendor_etc_file:dir { search };
allow resource_schedule_service vendor_etc_file:file { getattr map open read };
#system
allow resource_schedule_service system_basic_hap:dir { open read search };
allow resource_schedule_service system_basic_hap:file { getattr open read };
allow resource_schedule_service system_basic_hap:process { setsched sigkill };
allow resource_schedule_service system_bin_file:dir { search };
allow resource_schedule_service system_bin_file:file { execute execute_no_trans getattr map read open };
allow resource_schedule_service system_bin_file:lnk_file { read };
allow resource_schedule_service system_core_hap:dir { open read search };
allow resource_schedule_service system_core_hap:file { getattr open read };
allow resource_schedule_service system_core_hap:process { setsched };
allow resource_schedule_service system_fonts_file:dir { open read search };
allow resource_schedule_service system_fonts_file:file { open read };
allow resource_schedule_service system_usr_file:dir { search map };
allow resource_schedule_service system_usr_file:file { getattr read };
allow resource_schedule_service system_lib_file:dir { search };
allow resource_schedule_service system_lib_file:file { getattr map open read };
allow resource_schedule_service system_etc_file:dir { search };
allow resource_schedule_service system_etc_file:file { getattr map open read };
#tracefs
allow resource_schedule_service tracefs:dir { search };
allow resource_schedule_service tracefs_trace_marker_file:file { open write };
#dev
allow resource_schedule_service dev_console_file:chr_file { read write };
allow resource_schedule_service dev_unix_socket:dir { search };
allow resource_schedule_service dev_unix_socket:sock_file { write };
#ui_service
allow resource_schedule_service ui_service:process { setsched };
#normal_hap
allow resource_schedule_service normal_hap_attr:dir { open read search };
allow resource_schedule_service normal_hap_attr:file { getattr open read };
allow resource_schedule_service normal_hap_attr:process { setsched };
#unix_dgram_socket
allow resource_schedule_service resource_schedule_service:unix_dgram_socket { getopt setopt };
#data
allow resource_schedule_service data_file:dir { search };
allow resource_schedule_service data_init_agent:dir { search };
allow resource_schedule_service data_init_agent:file { ioctl open read append };
allow resource_schedule_service data_log:file { read write };
#faultloggerd
allow resource_schedule_service faultloggerd:fd { use };
allow resource_schedule_service faultloggerd:unix_stream_socket { connectto };
#init 
allow resource_schedule_service init:dir { search };
allow resource_schedule_service init:file { getattr open read }; 
#limit_domain
allow resource_schedule_service limit_domain:dir { search };
allow resource_schedule_service limit_domain:file { getattr open read };
#kernel
allow resource_schedule_service kernel:dir { search };
allow resource_schedule_service kernel:file { getattr open read };
allow resource_schedule_service kernel:key { search };
#lib_file
allow resource_schedule_service lib_file:lnk_file { read };
#musl_param
allow resource_schedule_service musl_param:file { open read map };
#netlink_socket
allow resource_schedule_service resource_schedule_service:netlink_socket { read };
#proc_file
allow resource_schedule_service proc_file:file { read open };
#param_watcher
allow resource_schedule_service param_watcher:file { read };
#sh
allow resource_schedule_service sh:dir { search };
allow resource_schedule_service sh:file { open };
#sh_exec
allow resource_schedule_service sh_exec:file { execute_no_trans map open read };
#sys_file
allow resource_schedule_service sys_file:file { getattr write open ioctl create read };
allow resource_schedule_service sys_file:dir { open read search };
#system_file
allow resource_schedule_service system_file:file { open read };
#sys_prod_file
allow resource_schedule_service sys_prod_file:dir { search };
allow resource_schedule_service sys_prod_file:file { open read };
#sysfs_devices_system_cpu
allow resource_schedule_service sysfs_devices_system_cpu:file { getattr write open ioctl create read };
allow resource_schedule_service sysfs_devices_system_cpu:dir { open read search };
#storage_daemon
allow resource_schedule_service storage_daemon:file { read };
#storage_manager
allow resource_schedule_service storage_manager:file { read };
#tty_device
allow resource_schedule_service tty_device:chr_file { open read write };
#frame_aware_scha
allow resource_schedule_service dev_sched_rtg_ctrl:chr_file { ioctl open read write };

#allowxperm
allowxperm resource_schedule_service dev_file:chr_file ioctl { 0x7102  0x7104 0x7165 };
allowxperm resource_schedule_service dev_sched_rtg_ctrl:chr_file ioctl { 0xab01 0xab02 };
allowxperm resource_schedule_service cgroup:file ioctl { 0x5413 };
allowxperm resource_schedule_service sys_file:file ioctl { 0x5413 };
allowxperm resource_schedule_service data_service_el1_file:file ioctl { 0x5413 0xf501 0xf502 0xf50c };

#sa_pulseaudio_audio_service
allow resource_schedule_service sa_pulseaudio_audio_service:samgr_class { get };
#sa_audio_policy_service
allow resource_schedule_service sa_audio_policy_service:samgr_class { get };
#samgr
allow resource_schedule_service samgr:file { read };
#ueventd
allow resource_schedule_service ueventd:dir { search };
allow resource_schedule_service ueventd:file { getattr open read };
#watchdog_service
allow resource_schedule_service watchdog_service:dir { search };
allow resource_schedule_service watchdog_service:file { getattr open read };
#sadomain
allow resource_schedule_service sadomain:process { sigkill };
allow resource_schedule_service resource_schedule_service:capability { kill };

allow resource_schedule_service sa_locationhub_lbsservice_gnss:samgr_class { get };
allow resource_schedule_service sa_locationhub_lbsservice_network:samgr_class { get };
allow resource_schedule_service sa_locationhub_lbsservice_passive:samgr_class { get };
allow resource_schedule_service sa_location_geo_convert_service:samgr_class { get };
