# Copyright (c) 2022-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

type devattest_service, sadomain, domain;
type devattest_service_exec, system_file_attr, exec_attr, file_attr;

init_daemon_domain(devattest_service);

allow storage_daemon hmdfs:dir { mounton };
allow foundation storage_manager:dir { open read write };
allow foundation storage_manager:file { open read write };
allow netsysnative netmanager:tcp_socket { create read write getopt setopt };

allow devattest_service data_file:dir { search };
allow devattest_service data_data_file:dir { search getattr add_name open read remove_name search write create };
allow devattest_service data_data_file:file { append map open read create write getattr setattr unlink lock ioctl rename };
allow devattest_service data_ota_package:dir { append ioctl open read add_name search write remove_name };
allow devattest_service data_ota_package:file { append create ioctl open read rename unlink };
allow devattest_service dev_file:sock_file { write };

allow devattest_service data_device_attest:dir { search getattr add_name open read remove_name write create };
allow devattest_service data_device_attest:file { append map open read create write getattr setattr unlink lock ioctl rename };

allow devattest_service netsysnative:unix_stream_socket { connectto };
allow devattest_service port:tcp_socket { name_connect };
allow devattest_service devattest_service:tcp_socket { connect create read setopt write getopt getattr };
allow devattest_service devattest_service:udp_socket { create bind connect getattr read write };

allow devattest_service accesstoken_service:binder { call };
allow devattest_service foundation:binder { call transfer };
allow devattest_service netmanager:binder { call transfer };
allow netmanager devattest_service:binder { call };
allow devattest_service softbus_server:binder { call };

allow devattest_service data_service_el1_file:dir { add_name remove_name search write create };
allow devattest_service data_service_el1_file:file { create getattr ioctl lock open read setattr unlink write };
allow devattest_service data_misc:dir { add_name search write };
allow devattest_service data_misc:file { create ioctl open read write };
allow devattest_service data_misc:sock_file { write };
allow devattest_service accessibility_param:file { read };
allow devattest_service dev_unix_socket:dir { search };
allow devattest_service system_bin_file:dir { search };
allow devattest_service system_bin_file:file { execute execute_no_trans map read open };

allow devattest_service node:udp_socket { node_bind };
allow devattest_service port:udp_socket { name_bind };
allow devattest_service wifi_hal_service:unix_stream_socket { connectto };
allow devattest_service kernel:unix_stream_socket { connectto };

allow devattest_service devattest_service:netlink_route_socket { create nlmsg_read read write };
allow devattest_service devattest_service:packet_socket { bind create read write };
allow devattest_service devattest_service:udp_socket { bind create ioctl setopt getopt read write };
allow devattest_service devattest_service:unix_dgram_socket { ioctl getopt setopt };
allowxperm devattest_service data_service_el1_file:file ioctl { 0x5413 };
allowxperm devattest_service data_misc:file ioctl { 0x5413 };
allowxperm devattest_service devattest_service:udp_socket ioctl { 0x890B 0x8913 0x8915 0x8916 0x891b 0x891c 0x8927 0x8933 };
allowxperm devattest_service devattest_service:unix_dgram_socket ioctl { 0x8910 };

allow devattest_service paramservice_socket:sock_file { write create setattr getattr relabelto };
allow devattest_service xts_devattest_authresult_param:file { map open read };
allow devattest_service xts_devattest_authresult_param:parameter_service { set };
allow param_watcher xts_devattest_authresult_param:parameter_service { set };
allow { domain -limit_domain } xts_devattest_authresult_param:file { map open read };

allow devattest_service sa_devattest_service:samgr_class { add };
allow devattest_service sa_net_conn_manager:samgr_class { get };
allow devattest_service sa_device_service_manager:samgr_class { get };
allow devattest_service sa_accesstoken_manager_service:samgr_class { add get };
allow devattest_service sa_foundation_bms:samgr_class { get };

allow devattest_service devinfo_private_param:file { map open read };

allow devattest_service musl_param:file { read };
allow devattest_service hilog_param:file { map open read };
allow devattest_service dnsproxy_service:sock_file { write };

# [18.469899] audit: type=1400 audit(1668560965.423:331): avc:  denied  { call } for  pid=349 comm="netmanager" scontext=u:r:netmanager:s0 tcontext=u:r:devattest_service:s0 tclass=binder permissive=0
allow netmanager devattest_service:binder { call };

allow normal_hap_attr sa_devattest_service:samgr_class { get };
allow normal_hap_attr devattest_service:fd { use };
allow normal_hap_attr devattest_service:binder { call transfer };
allow devattest_service normal_hap_attr:binder { call transfer };

allow system_basic_hap sa_devattest_service:samgr_class { get };
allow system_basic_hap devattest_service:fd { use };
allow system_basic_hap devattest_service:binder { call transfer };
allow devattest_service system_basic_hap:binder { call transfer };

allow system_core_hap sa_devattest_service:samgr_class { get };
allow system_core_hap devattest_service:fd { use };
allow system_core_hap devattest_service:binder { call transfer };
allow devattest_service system_core_hap:binder { call transfer };
